import os
import json
import gc

import numpy as np
import networkx as nx

import warnings
warnings.filterwarnings('ignore')

gc.collect()

GRAPH_DATA_FOLDER = "graphs/"

KEYWORDS = ["ClassDeclaration", "EnumDeclaration", "EnumConstantDeclaration", "InterfaceDeclaration", "AnnotationDeclaration", "Annotation", "MethodDeclaration", "FieldDeclaration", "ConstructorDeclaration", "ConstantDeclaration", "ArrayInitializer", "VariableDeclaration", "LocalVariableDeclaration", "VariableDeclarator", "IfStatement", "WhileStatement", "DoStatement", "ForStatement", "AssertStatement", "BreakStatement", "ContinueStatement", "ReturnStatement", "ThrowStatement", "SynchronizedStatement", "TryStatement", "SwitchStatement", "BlockStatement", "MethodReference", "MemberReference", "SuperMemberReference", "ClassReference", "VoidClassReference", "ExplicitConstructorInvocation", "SuperConstructorInvocation", "MethodInvocation", "SuperMethodInvocation", "ArrayCreator", "ClassCreator", "InnerClassCreator"]
KEYWORDS = dict(zip(['0'] + KEYWORDS, [i for i in range(len(KEYWORDS) + 1)]))

if not os.path.exists(GRAPH_DATA_FOLDER):
    os.makedirs(GRAPH_DATA_FOLDER)


def load_graph_data(graph, filename, TRAIN_PROJECT_NAME):
    with open(os.path.join(os.path.join(os.path.join(GRAPH_DATA_FOLDER, "nodes"), TRAIN_PROJECT_NAME), filename + ".json")) as f:
        nodes = json.load(f)
        graph.add_nodes_from(nodes)

    with open(os.path.join(os.path.join(os.path.join(GRAPH_DATA_FOLDER, "edges"), TRAIN_PROJECT_NAME), filename + ".json")) as f:
        edges = [tuple(i) for i in json.load(f)]
        graph.add_edges_from(edges)

    with open(os.path.join(os.path.join(os.path.join(GRAPH_DATA_FOLDER, "features"), TRAIN_PROJECT_NAME), filename + ".json")) as f:
        features = json.load(f)
        for node in graph.nodes():
            graph.nodes[node]['feature'] = KEYWORDS[features[str(node)]]


def make_dataset(graph_data_folder, project_name_, keywords):

    project_name = project_name_.split("-")[0]
    project_name = os.path.join(project_name, project_name_.split("-")[-1].replace(".", "-"))
    for filepath in os.listdir(os.path.join(os.path.join(graph_data_folder, "nodes"), project_name)):
        
        filename = ".".join(filepath.split(".")[:-1])
        graph = nx.Graph()
        
        load_graph_data(graph, filename, project_name)
        
        A = nx.to_scipy_sparse_matrix(graph)
        
        X = [graph.nodes()[n]["feature"] for n in range(len(graph.nodes()))]
        n_values = len(keywords)
        X = np.eye(n_values)[X]
        
        A = A.todense()       
        A[np.isnan(A)] = 0
        X[np.isnan(X)] = 0
        
        if not os.path.exists(os.path.join(GRAPH_DATA_FOLDER, project_name_.split("-")[0])):
            os.mkdir(os.path.join(GRAPH_DATA_FOLDER, project_name_.split("-")[0]))
        if not os.path.exists(os.path.join(GRAPH_DATA_FOLDER, project_name)):
            os.mkdir(os.path.join(GRAPH_DATA_FOLDER, project_name))


        np.save(os.path.join(os.path.join(GRAPH_DATA_FOLDER, project_name), "A_" + filepath.replace(".nodes", "")), A)
        np.save(os.path.join(os.path.join(GRAPH_DATA_FOLDER, project_name), "X_" + filepath.replace(".nodes", "")), X)


gc.collect()

for TRAIN_PROJECT_NAME, TEST_PROJECT_NAME in zip(
    ["camel-1.4", "jedit-4.0", "lucene-2.0", "poi-2.5", "synapse-1.1", "xalan-2.5", "xerces-1.2"],
    ["camel-1.6", "jedit-4.1", "lucene-2.2", "poi-3.0", "synapse-1.2", "xalan-2.6", "xerces-1.3"]):
    
    make_dataset(GRAPH_DATA_FOLDER, TRAIN_PROJECT_NAME, KEYWORDS)
    make_dataset(GRAPH_DATA_FOLDER, TEST_PROJECT_NAME, KEYWORDS)


