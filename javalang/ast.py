import pickle
import sys
import six

this = sys.modules[__name__]
this.d = []
this.features = {}

this.stari_d = []
this.stare_f = {}

class MetaNode(type):
    def __new__(mcs, name, bases, dict):
        attrs = list(dict['attrs'])
        dict['attrs'] = list()

        for base in bases:
            if hasattr(base, 'attrs'):
                dict['attrs'].extend(base.attrs)

        dict['attrs'].extend(attrs)

        return type.__new__(mcs, name, bases, dict)


@six.add_metaclass(MetaNode)
class Node(object):
    attrs = ()

    def __init__(self, **kwargs):
        values = kwargs.copy()

        for attr_name in self.attrs:
            value = values.pop(attr_name, None)
            setattr(self, attr_name, value)

        if values:
            raise ValueError('Extraneous arguments')

    def __equals__(self, other):
        if type(other) is not type(self):
            return False

        for attr in self.attrs:
            if getattr(other, attr) != getattr(self, attr):
                return False

        return True

    def __repr__(self):
        attr_values = []
        for attr in sorted(self.attrs):
            attr_values.append('%s=%s' % (attr, getattr(self, attr)))
        return '%s(%s)' % (type(self).__name__, ', '.join(attr_values))

    def __iter__(self):
        return walk_tree(self, None)

    def filter(self, pattern):
        for path, node in self:
            if ((isinstance(pattern, type) and isinstance(node, pattern)) or
                (node == pattern)):
                yield path, node

    @property
    def children(self):
        return [getattr(self, attr_name) for attr_name in self.attrs]
    
    @property
    def position(self):
        if hasattr(self, "_position"):
            return self._position

GLOBAL_DICT = dict()

def walk_tree(root, old=None):
    children = None
    if isinstance(root, Node):
        yield (), root
        children = root.children
    else:
        children = root

    if not old:
        this.stare_f[str(old)] = "Root"

    node_type = str(root).strip("[").strip("]").split("(")[0].strip()
    this.stare_f[str(root)] = node_type.split("Statement")[0]

    real_name = str(root)
    if real_name != str(old):
        to_write = str(old) + "###" + real_name
        if to_write not in this.stari_d:
            this.stari_d.append(to_write)
    
    for child in children:
        if str(root) not in GLOBAL_DICT:
            GLOBAL_DICT[str(root)] = []
        GLOBAL_DICT[str(root)].append(child)
        #print("DA")
        #print("DIJETE:", child)
        if isinstance(child, (Node, list, tuple)):
            for path, node in walk_tree(child, str(root)):
                yield (root,) + path, node


def get_real():    
    cnt = 0
    rj = {}
    new_list, new_dict = [], dict()
    for pair in this.stari_d:
        a, b = pair.split("###")
        if a not in rj:
            new_dict[cnt] = this.stare_f[a]
            rj[a] = cnt
            cnt += 1
        if b not in rj:
            new_dict[cnt] = this.stare_f[b]
            rj[b] = cnt
            cnt += 1
        if str(rj[a]) + " " + str(rj[b]) not in new_list:
            new_list.append(str(rj[a]) + " " + str(rj[b]))
    this.d = new_list
    this.features = new_dict



def dump(ast, file):
    pickle.dump(ast, file)

def load(file):
    return pickle.load(file)
