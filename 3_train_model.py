import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import gc

import numpy as np

import tensorflow as tf
from tensorflow.keras.layers import *
from tensorflow.keras.models import Model

from spektral.layers import MinCutPool, ChebConv, GlobalAvgPool

from tensorflow.keras.optimizers import *
from tensorflow.keras.regularizers import l2


from sklearn.metrics import roc_auc_score, f1_score

import warnings
warnings.filterwarnings('ignore')

os.environ['CUDA_VISIBLE_DEVICES'] = '0,1'
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

if tf.test.gpu_device_name():
    print('GPU found')
else:
    print("No GPU found")



TEXT_FOLDER = "text/"
GRAPH_DATA_FOLDER = "graphs/"

KEYWORDS = ["ClassDeclaration", "EnumDeclaration", "EnumConstantDeclaration", "InterfaceDeclaration", "AnnotationDeclaration", "Annotation", "MethodDeclaration", "FieldDeclaration", "ConstructorDeclaration", "ConstantDeclaration", "ArrayInitializer", "VariableDeclaration", "LocalVariableDeclaration", "VariableDeclarator", "IfStatement", "WhileStatement", "DoStatement", "ForStatement", "AssertStatement", "BreakStatement", "ContinueStatement", "ReturnStatement", "ThrowStatement", "SynchronizedStatement", "TryStatement", "SwitchStatement", "BlockStatement", "MethodReference", "MemberReference", "SuperMemberReference", "ClassReference", "VoidClassReference", "ExplicitConstructorInvocation", "SuperConstructorInvocation", "MethodInvocation", "SuperMethodInvocation", "ArrayCreator", "ClassCreator", "InnerClassCreator"]
KEYWORDS = dict(zip(['0'] + KEYWORDS, [i for i in range(len(KEYWORDS) + 1)]))


def load_data(project_name):
    
    A_ret, X_ret, y_ret = [], [], []
    
    global TEXT_FOLDER
    
    file2label = None
    with open(os.path.join(TEXT_FOLDER, project_name + ".txt"), "r") as f:
        lines = [l.strip() for l in f.readlines()]
        file2label = dict(zip([i.split("\t")[0] for i in lines], [int(i.split("\t")[1]) for i in lines]))
        
    for filepath in os.listdir(os.path.join(GRAPH_DATA_FOLDER, project_name.replace("-", "/").replace(".", "-")))[::-1]:
        if "A_" in filepath:
            A = np.load(os.path.join(os.path.join(GRAPH_DATA_FOLDER, project_name.replace("-", "/").replace(".", "-")), filepath), encoding='latin1', allow_pickle=True)
            X = np.load(os.path.join(os.path.join(GRAPH_DATA_FOLDER, project_name.replace("-", "/").replace(".", "-")), filepath.replace("A_", "X_")), encoding='latin1', allow_pickle=True)
            A_ret.append(A)
            X_ret.append(X)
            y = file2label[filepath.replace(".npy", "").replace("A_", "").replace(".json", "")]
            y_ret.append(y)
            
    return A_ret, X_ret, y_ret


def create_DPCGNN_model(node_number, node_features):
    
    X_in = Input(shape=(node_number, node_features))
    A_in = Input(shape=(node_number, node_number))

    X_in_ = Dropout(0.5)(X_in)    
    
    X_1 = ChebConv(node_number, K=3, activation="elu", kernel_regularizer=l2(5e-5))([X_in_, A_in])
    X_1, A_1 = MinCutPool(node_number // 2)([X_1, A_in])
    X_1 = Dropout(0.5)(X_1)
    
    X_2 = ChebConv(node_number // 2, K=3, activation="elu", kernel_regularizer=l2(5e-5))([X_1, A_1])
    X_2, A_2 = MinCutPool(node_number // 4)([X_2, A_1])
    X_2 = Dropout(0.5)(X_2)    
        
    X_3 = ChebConv(node_number // 4, K=3, activation="elu", kernel_regularizer=l2(5e-5))([X_2, A_2])
    X_3, A_3 = MinCutPool(node_number // 8)([X_3, A_2])
    X_3 = Dropout(0.5)(X_3)
    
    last_stage = GlobalAvgPool()(X_3)
    last_stage = Dropout(0.5)(last_stage)
    last_stage = Dense(node_number // 16, kernel_initializer='he_normal', activation="elu")(last_stage)
    output = Dense(1, kernel_initializer='he_normal')(last_stage)
    output = Activation('sigmoid')(output)
    
    model = Model(inputs=[X_in, A_in], outputs=output)
    return model


def prepare_data(data):
    global KEYWORDS
    
    batch, max_nodes = data[0], data[-1]
    
    A, X = [data[1][i] for i in batch], [data[2][i] for i in batch]
    y = [data[3][i] for i in batch]
    
    X_lil, A_lil, = [], []
    
    for i in range(len(A)):
        target = np.zeros((max_nodes, max_nodes))
        target[:A[i].shape[0], :A[i].shape[1]] = A[i]
        
        target[np.isnan(target)] = 0
        
        A_lil.append(target)
        
        target = np.zeros((max_nodes, len(KEYWORDS)))
        target[:X[i].shape[0], :X[i].shape[1]] = X[i]
        
        target[np.isnan(target)] = 0
        
        X_lil.append(target)
    
    return X_lil, A_lil, y


def make_batches(y, N, perc):
    batches = []
    
    NR_ITERS = int(len(y)/N)
    if (len(y) % N != 0):
        NR_ITERS += 1
    
    neg_indices = np.argwhere(np.array(y) < 1).flatten()
    pos_indices = np.argwhere(np.array(y) == 1).flatten()
    
    negative = range(int(float(len(neg_indices)/len(y)) * N))
    positive = range(int(float(len(pos_indices)/len(y)) * N))
    
    if len(neg_indices) < len(pos_indices):
        negative = range(int(N * perc)) 
        positive = range(int(N * (1-perc))) 
    else:
        negative = range(int(N * (1-perc))) 
        positive = range(int(N * perc)) 

    for i in range(NR_ITERS):
        ret = [neg_indices[(i*len(negative) + n)%len(neg_indices)] for n in negative]
        ret.extend([pos_indices[(i*len(positive) + p)%len(pos_indices)] for p in positive])
        
        batches.append(ret)
        
    return batches


gc.collect()
UBER_BATCH_SIZE = 2

for TRAIN_PROJECT_NAME, TEST_PROJECT_NAME in zip(
    ["camel-1.4", "jedit-4.0", "lucene-2.0", "poi-2.5", "synapse-1.1", "xalan-2.5", "xerces-1.2"],
    ["camel-1.6", "jedit-4.1", "lucene-2.1", "poi-3.0", "synapse-1.2", "xalan-2.6", "xerces-1.3"]):

     
    project_path = TRAIN_PROJECT_NAME.split("-")[0] 
    print(project_path)
    
    lr, patience, batch_size = 1e-4, 5, UBER_BATCH_SIZE          

    gc.collect()
    
    A_train, X_train, y_train = load_data(TRAIN_PROJECT_NAME)
    A_test, X_test, y_test = load_data(TEST_PROJECT_NAME)

    #class_weights = class_weight.compute_class_weight('balanced', np.unique(y_train), y_train)
    #class_weights = dict(enumerate(class_weights))

    batches = make_batches(y_train, UBER_BATCH_SIZE, perc=0.5)
    test_batches = [[j for j in range(UBER_BATCH_SIZE*i, UBER_BATCH_SIZE*(i+1), 1) if j < len(y_test)] for i in range(int(len(y_test)/UBER_BATCH_SIZE) + 1)]
    test_batches = [b for b in test_batches if len(b) > 0]
   
    max_nodes = max([max([i.shape[0] for i in A_train]), max([i.shape[0] for i in A_test])])

    cnt, temp_val = 0, 0
    for epoch in range(200):
        X_lil_val, A_lil_val, y_val = prepare_data([batches[-1], A_train, X_train, y_train, max_nodes])

        for n, batch in enumerate(batches[:-1]):

            X_lil, A_lil, y = prepare_data([batch, A_train, X_train, y_train, max_nodes])

            if n == 0 and epoch == 0:
                model = create_DPCGNN_model(np.array(X_lil[0]).shape[0], np.array(X_lil[0]).shape[1])
                model.compile(optimizer=Adam(lr=lr), loss='binary_crossentropy')

            model.train_on_batch([np.array(X_lil), np.array(A_lil)], np.array(y))
            #model.fit([np.array(X_lil), np.array(A_lil)], np.array(y), validation_data=([np.array(X_lil_val), np.array(A_lil_val)], y_val), class_weight=class_weights, verbose=0, callbacks=[es]) 

            y_pred = model.predict([np.array(X_lil_val), np.array(A_lil_val)])
            y_pred = [_[0] for _ in y_pred]
            y_pred = [0 if yy < 0.5 else 1 for yy in y_pred]
            score_valid = f1_score(y_true=y_val, y_pred=y_pred)

            if np.abs(score_valid - temp_val) < 1e-5:
                cnt += 1
                if cnt == 20:
                    break

        del X_lil, A_lil, y, X_lil_val, A_lil_val, y_val

        gc.collect()


        y_pred, y_true = [], []
        for n, test_batch in enumerate(test_batches):            
            X_lil, A_lil, y = prepare_data([test_batch, A_test, X_test, y_test, max_nodes])
            y_p = model.predict([np.array(X_lil), np.array(A_lil)])
            y_p = [_[0] for _ in y_p]
            y_p = [0 if yy < 0.5 else 1 for yy in y_p]
            y_pred.extend(y_p)
            y_true.extend(y)
            del X_lil, A_lil, y


        print("auc_test_score:", roc_auc_score(y_true=y_true, y_score=y_pred))
        print("f1_test_score: ", f1_score(y_true=y_true, y_pred=y_pred))
        print()
        del y_true, y_pred
        gc.collect()

    print()