import os
import json
import gc

import networkx as nx

import warnings
warnings.filterwarnings('ignore')

gc.collect()

JSON_FOLDER = "json/"
GRAPHS_FOLDER = "graphs"


KEYWORDS = ["ClassDeclaration", "EnumDeclaration", "EnumConstantDeclaration", "InterfaceDeclaration", "AnnotationDeclaration", "Annotation", "MethodDeclaration", "FieldDeclaration", "ConstructorDeclaration", "ConstantDeclaration", "ArrayInitializer", "VariableDeclaration", "LocalVariableDeclaration", "VariableDeclarator", "IfStatement", "WhileStatement", "DoStatement", "ForStatement", "AssertStatement", "BreakStatement", "ContinueStatement", "ReturnStatement", "ThrowStatement", "SynchronizedStatement", "TryStatement", "SwitchStatement", "BlockStatement", "MethodReference", "MemberReference", "SuperMemberReference", "ClassReference", "VoidClassReference", "ExplicitConstructorInvocation", "SuperConstructorInvocation", "MethodInvocation", "SuperMethodInvocation", "ArrayCreator", "ClassCreator", "InnerClassCreator"]
KEYWORDS = dict(zip(['0'] + KEYWORDS, [i for i in range(len(KEYWORDS) + 1)]))

if not os.path.exists(GRAPHS_FOLDER):
    os.makedirs(GRAPHS_FOLDER)


def make_graphs(filepath, project_name, project_version):
    filefolder = os.path.join(JSON_FOLDER, os.path.join(project_name, project_version))
    with open(os.path.join(filefolder, filepath + ".json")) as f:

        data = json.load(f)
            
        graph = nx.from_edgelist(data["edges"])
        features = {int(k):{"feature":data["features"][k]} for k in data["features"].keys()}
        nx.set_node_attributes(graph, features)

        with open(os.path.join("graphs", os.path.join("edges", os.path.join(os.path.join(project_name, project_version.replace(".", "-")), filepath + ".json"))), 'w') as f:
            json.dump(list(graph.edges()), f)
        
        with open(os.path.join("graphs", os.path.join("nodes", os.path.join(os.path.join(project_name, project_version.replace(".", "-")), filepath + ".json"))), 'w') as f:
            json.dump(list(graph.nodes()), f)
        
        with open(os.path.join("graphs", os.path.join("features", os.path.join(os.path.join(project_name, project_version.replace(".", "-")), filepath + ".json"))), 'w') as f:
            to_save = dict()
            for node in graph.nodes():
                to_save[node] = graph.nodes[node]['feature']
            json.dump(to_save, f)


for PROJECT in ["camel-1.4", "jedit-4.0", "lucene-2.0", "poi-2.5", "synapse-1.1", "xalan-2.5", "xerces-1.2", "camel-1.6", "jedit-4.1", "lucene-2.2", "poi-3.0", "synapse-1.2", "xalan-2.6", "xerces-1.3"]:
    
    project_name, project_version = PROJECT.split("-")
    for filepath in os.listdir(os.path.join(JSON_FOLDER, os.path.join(project_name, project_version))):
    
        if not os.path.exists(os.path.join(os.path.join(GRAPHS_FOLDER, "edges"), project_name)):
            os.mkdir(os.path.join(os.path.join(GRAPHS_FOLDER, "edges"), project_name))
            os.mkdir(os.path.join(os.path.join(GRAPHS_FOLDER, "nodes"), project_name))
            os.mkdir(os.path.join(os.path.join(GRAPHS_FOLDER, "features"), project_name))
        if not os.path.exists((os.path.join(os.path.join(os.path.join(GRAPHS_FOLDER, "edges"), project_name), project_version.replace(".", "-")))):
            os.mkdir(os.path.join(os.path.join(os.path.join(GRAPHS_FOLDER, "edges"), project_name), project_version.replace(".", "-")))
            os.mkdir(os.path.join(os.path.join(os.path.join(GRAPHS_FOLDER, "nodes"), project_name), project_version.replace(".", "-")))
            os.mkdir(os.path.join(os.path.join(os.path.join(GRAPHS_FOLDER, "features"), project_name), project_version.replace(".", "-")))
        
        make_graphs(filepath.split(".json")[0].strip(), project_name, project_version)