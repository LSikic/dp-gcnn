import warnings
warnings.filterwarnings('ignore') 
import sys
import os

if not sys.warnoptions:
    warnings.simplefilter("ignore")
    os.environ["PYTHONWARNINGS"] = "ignore"

import os
import numpy as np
import pandas as pd
from sklearn.metrics import roc_auc_score, f1_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier as RFC
from sklearn.linear_model import LogisticRegression as LRC

DSF = "traditional_metrics_projects"
GRAPHS = "graphs"

n_estimators = [int(x) for x in np.linspace(start = 200, stop = 2000, num = 10)]
max_depth = [int(x) for x in np.linspace(10, 110, num = 11)]
max_depth.append(None)

random_grid = {'n_estimators': n_estimators,
               'max_depth': max_depth}

#random_grid = {'solver':['newton-cg', 'lbfgs', 'sag'],
#                'penalty':['none', 'l2'],
#                'C':loguniform(1e-5, 100)}


def fill_data(name, X, y):

    NAMES_1 = []

    for nm in os.listdir(os.path.join(GRAPHS, name)):
        if "A_" in nm:
            NAMES_1.append(nm.split("A_")[-1].replace(".npy", "").replace(".json", ""))

    nme = "\\".join(name.split("\\")[:-1]) + "-" + name.split("\\")[-1].replace("-", ".")
    df = pd.read_csv(os.path.join(DSF, nme + ".csv"))
    NAMES_2 = [i for i in df.iloc[:,2].values]
    XX = [i for i in df.iloc[:,3:-1].values]
    yy = [int(i) for i in df.iloc[:,-1].values]
    yy = [0 if i == 0 else 1 for i in yy]

    for nm in NAMES_1:
        if nm in NAMES_2:
            ind = NAMES_2.index(nm)
            X.append(XX[ind])
            y.append(yy[ind])


for aa, bb in zip(["camel-1.4", "jedit-4.0", "lucene-2.0", "poi-2.5", "synapse-1.1", "xalan-2.5", "xerces-1.2"],
                    ["camel-1.6", "jedit-4.1", "lucene-2.2", "poi-3.0", "synapse-1.2", "xalan-2.6", "xerces-1.3"]):

    print(aa.split("-")[0])
    TRAIN_SET_NAME, TEST_SET_NAME = aa, bb

    X_train, y_train = [], []
    X_test, y_test = [], []

    fill_data(TRAIN_SET_NAME.replace("-", "\\").replace(".", "-"), X_train, y_train)
    fill_data(TEST_SET_NAME.replace("-", "\\").replace(".", "-"), X_test, y_test)

    RFC_F, RFC_A = [], []

    for i in range(100):
        model = RFC(class_weight='balanced')
        grid_search = RandomizedSearchCV(model, random_grid, cv = 5, n_jobs = -1, verbose = 0)

        grid_search.fit(X_train, y_train)
        best_model = grid_search.best_estimator_
        y_pred = best_model.predict(X_test)
        f = f1_score(y_true=y_test, y_pred=y_pred)
        a = roc_auc_score(y_true=y_test,y_score=y_pred)
        RFC_F.append(f)
        RFC_A.append(a)

    RFC_A.sort()
    print("rfc auc:", RFC_A, np.mean(np.array(RFC_A)))

    RFC_F.sort()
    print("rfc f-s:", RFC_F, np.mean(np.array(RFC_F)))
